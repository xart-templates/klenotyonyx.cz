jQuery.noConflict();
jQuery(document).ready(function($){



	$('html').addClass('js');



	$('.search_header .toggle').on('click',function(){
		$('body').toggleClass('search_header-on');
	});



	$('.menu_main .toggle').on('click',function(){
		$('body').toggleClass('menu_main-on');
	});




	$('.carousel_slider').each(function(){
		$('ul',this).append('<a class="prev_slider"/><a class="next_slider"/>');
		$('ul',this).cycle({
			fx: 'carousel',
			slides: 'li',
			speed: 300,
			timeout: 25000,
			swipe: true,
			prev: $('.prev_slider',this),
			next: $('.next_slider',this),
		});
	});




	$('.thumbnails_slider').each(function(){
		var pager = $('.inner ul').html();
		$('.inner').after('<div class="pager"><ul>'+pager+'</ul></div><a class="prev"/><a class="next"/>');
		$('.pager ul',this).cycle({
			fx: 'carousel',
			carouselVertical: true,
			carouselVisible: 3,
			slides: 'li',
			speed: 300,
			timeout: 0,
			swipe: true,
			allowWrap: false,
			prev: $('.prev',this),
			next: $('.next',this),
		});
		$('.inner ul',this).cycle({
			fx: 'scrollHorz',
			slides: 'li',
			speed: 300,
			timeout: 0,
			swipe: true,
			allowWrap: false,
			pager: $('.pager .cycle-carousel-wrap',this),
			pagerTemplate: '',
			prev: $('.prev',this),
			next: $('.next',this),
		});
	});




	$('.product_list.slider').each(function(){
		$('.items',this).append('<a class="prev_slider"/><a class="next_slider"/>');
		$('.items',this).cycle({
			fx: 'carousel',
			slides: '.item',
			speed: 300,
			timeout: 15000,
			swipe: true,
			prev: $('.prev_slider',this),
			next: $('.next_slider',this),
		});
	});




	$('.slider_range').each(function(){
		$(this).wrapInner('<div class="slider"/>');
		$('input',this).hide();
		var data_min = $(this).data('min');
		var data_max = $(this).data('max');
		var data_step = $(this).data('step');
		var data_val = $(this).data('val');
		var input_from = $('[data-input="from"]',this).val();
		var input_to = $('[data-input="to"]',this).val();
		$('.slider',this).slider({
			range: true,
			min: data_min,
			max: data_max,
			step: data_step,
			values: [input_from,input_to],
			slide: function(event,ui) {
				$('.ui-slider-handle:first span',this).text(ui.values[0]+' '+data_val);
				$('.ui-slider-handle:last span',this).text(ui.values[1]+' '+data_val);
				$('[data-input="from"]',this).val(ui.values[0]);
				$('[data-input="to"]',this).val(ui.values[1]);
			}
		});
		$('.ui-slider-handle',this).prepend('<span/>');
		$('.ui-slider-handle:first span',this).text($('.slider',this).slider('values',0)+' '+data_val);
		$('.ui-slider-handle:last span',this).text($('.slider',this).slider('values',1)+' '+data_val);
	});



	// file input style
	$('input[type=file]').each(function(){
		var uploadText = $(this).data('value');
		if (uploadText) {
			var uploadText = uploadText;
		} else {
			var uploadText = 'Choose file';
		}
		var inputClass=$(this).attr('class');
		$(this).wrap('<span class="fileinputs"></span>');
		$(this)
			.parents('.fileinputs')
			.append($('<span class="fakefile"/>')
			.append($('<input class="input-file-text" type="text" />')
			.attr({'id':$(this)
			.attr('id')+'__fake','class':$(this).attr('class')+' input-file-text'}))
			.append('<input type="button" value="'+uploadText+'">'));
		$(this)
			.addClass('type-file')
			.css('opacity',0);
		$(this)
			.bind('change mouseout',function(){$('#'+$(this).attr('id')+'__fake')
			.val($(this).val().replace(/^.+\\([^\\]*)$/,'$1'))
		})
	});



	// placeholder
	if (document.createElement('input').placeholder==undefined){
		$('[placeholder]').focus(function(){
			var input=$(this);
			if(input.val()==input.attr('placeholder')){
				input.val('');
				input.removeClass('placeholder')
			}
		}).blur(function(){
			var input=$(this);
			if(input.val()==''||input.val()==input.attr('placeholder')){
				input.addClass('placeholder');
				input.val(input.attr('placeholder'))
			}
		}).blur()
	}



	// go top
	var go_top = function(){
		var offset = $(window).scrollTop();
		if(offset>100){
			$('#gotop').addClass('on');
		} else {
			$('#gotop').removeClass('on');
		}
	}
	go_top();
	$(window).scroll(function(){
		go_top();
	});
	$('#gotop a').click(function(){ 
		$('html,body').animate({scrollTop:0},600);
		return false;
	});



	// iCheck
	$('input[type="checkbox"],input[type="radio"]').iCheck();



	// selectBoxIt
	$('select:not([multiple]').selectBoxIt();

	


	// basictable
	$('table.normal').each(function(index, el) {
		// větší tabulka - povol zalamování
		if ($(this).find('th').length > 1)
		{
			$(this).basictable({
				tableWrapper: true
			});
		}
		// menší tabulka - nastav breakpoint na malý rozměr
		else
		{
			$(this).basictable({
				tableWrapper: true,
				breakpoint: 120 // css media query
			});
		}
	});



	/* funkce vykonané na připravenost dokumentu */
	onDocumentReady($);



	/* vyčká, než je dokončena jakakoliv událost */
	var waitForFinalEvent = (function () {
		var timers = {};
		return function (callback, ms, uniqueId) {
			if (!uniqueId) {
				uniqueId = "Don't call this twice without a uniqueId";
			}
			if (timers[uniqueId]) {
				clearTimeout (timers[uniqueId]);
			}
			timers[uniqueId] = setTimeout(callback, ms);
		};
	})();



	/* na opravdovou zmenu velikosti okna - konej */
	window._width = $(window).width();
	$(window).resize(function () {
		waitForFinalEvent(function(){

			// detekce skutečné změny
			if (window._width != $(window).width())
			{
				window._width = $(window).width();
				onWindowResize($);
			}

		}, 500, "afterWindowResize");
	});

});


/**
 * funkce vykonané na připravenost dokumentu
 *
 * @param   {function}  jQ  objekt jQuery
 * 
 * @return
 */
function onDocumentReady(jQ)
{
	uniteHeights(jQ);				// vyrovnej výšky elementů
	return;
}


/**
 * funkce vykonané na responzivní chování
 *
 * @param   {function}  jQ 	objekt jQuery
 *
 * @return
 */
function onWindowResize(jQ)
{
	uniteHeights(jQ);				// vyrovnej výšky elementů
	return;
}


/**
 * Funkce pro sjednocování elementů stránky
 * - na základě třídy (unite-heights)
 * a nastavené ID skupiny elementů (data-ug)
 *
 * @param   {function}  jQ 	objekt jQuery
 *
 * @return
 */
function uniteHeights(jQ)
{
	// seznam skupin
	var u_gs = [];
	jQ('.unite-heights').each(function(i, el) {
		var c_ug = jQ(this).attr('data-ug');
		if (jQ.inArray( c_ug, u_gs ) == -1)
		{
			u_gs.push(c_ug);
		}
	});

	// projdi všechny skupiny a nastav jim stejné výšky
	if (u_gs.length > 0)
	{
		jQ.each(u_gs, function(i, g_id) {

			var heighest = 0;
			s = '[data-ug="' + g_id + '"]';	// data unite group

			// zresetuj a načti nejvyšší
			jQ(s).each(function(index, el) {
				jQ(this).css('height', 'auto');	// reset
				heighest = jQ(this).height() > heighest ? jQ(this).height() : heighest;
			});

			// nastav nejvyšší
			jQ(s).each(function(index, el) {
				jQ(this).height(heighest);
			});
		});
	}
	return;
}